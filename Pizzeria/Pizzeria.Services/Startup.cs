﻿using System;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pizzeria.Services.Interfaces;
using Pizzeria.Services.Profiles;
using Pizzeria.Services.Services;

namespace Pizzeria.Services
{
    public static class Startup
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection serviceCollection,
            IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            return serviceCollection
                .AddScoped<IPizzaService, PizzaService > ()
                .AddScoped<IOrderService, OrderService>()
                .AddScoped<IIngredientService, IngredientService>()
                .AddAutoMapper(c => c.AddProfiles(new Profile[] { new PizzaProfile(), new IngredientProfile(), new OrderProfile() }));
        }
    }
}
