﻿using AutoMapper;
using Pizzeria.DataAccess.Entities;
using Pizzeria.DataAccess.Interfaces;
using Pizzeria.Services.Interfaces;
using Pizzeria.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.Services.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Add(OrderDto model)
        {
            var order = _mapper.Map<Order>(model);
            _unitOfWork.OrderRepository.AddAsync(order);
        }

        public IEnumerable<OrderDto> GetAll()
        {
            var orders = _unitOfWork.OrderRepository.FindAll().ToList();

            return _mapper.Map<IEnumerable<OrderDto>>(orders);
        }

        public Task<OrderDto> GetByIdAsync(string id)
        {
            var order = _unitOfWork.OrderRepository.GetByIdAsync(id).Result;

            return Task.FromResult(_mapper.Map<OrderDto>(order));
        }

        public void Update(OrderDto model)
        {
            throw new System.NotImplementedException();
        }
    }
}
