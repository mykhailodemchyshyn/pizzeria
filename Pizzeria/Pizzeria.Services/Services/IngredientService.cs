﻿using AutoMapper;
using Pizzeria.DataAccess.Entities;
using Pizzeria.DataAccess.Interfaces;
using Pizzeria.Services.Interfaces;
using Pizzeria.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.Services.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public IngredientService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Add(IngredientDto model)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<IngredientDto> GetAll()
        {
            var ingredients = _unitOfWork.IngredientRepository.FindAll().ToList();

            return _mapper.Map<IEnumerable<IngredientDto>>(ingredients);
        }

        public Task<IngredientDto> GetByIdAsync(string id)
        {
            var ingredient = _unitOfWork.IngredientRepository.GetByIdAsync(id).Result;

            return Task.FromResult(_mapper.Map<IngredientDto>(ingredient));
        }

        public void Update(IngredientDto model)
        {
            var ingredient = _mapper.Map<Ingredient>(model);
            _unitOfWork.IngredientRepository.Update(ingredient);

        }
    }
}
