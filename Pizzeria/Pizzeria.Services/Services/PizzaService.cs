﻿using AutoMapper;
using Pizzeria.DataAccess.Interfaces;
using Pizzeria.Services.Interfaces;
using Pizzeria.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.Services.Services
{
    public class PizzaService : IPizzaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PizzaService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Add(PizzaDto model)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PizzaDto> GetAll()
        {
            var pizzas = _unitOfWork.PizzaRepository.FindAll().ToList();
            
            return _mapper.Map<IEnumerable<PizzaDto>>(pizzas);
        }

        public Task<PizzaDto> GetByIdAsync(string id)
        {
            var pizza = _unitOfWork.PizzaRepository.GetByIdAsync(id).Result;

            return Task.FromResult(_mapper.Map<PizzaDto>(pizza));
        }

        public void Update(PizzaDto model)
        {
            throw new NotImplementedException();
        }
    }
}
