﻿using AutoMapper;
using Pizzeria.DataAccess.Entities;
using Pizzeria.Services.Models;

namespace Pizzeria.Services.Profiles
{
    public class IngredientProfile : Profile
    {
        public IngredientProfile()
        {
            CreateMap<Ingredient, IngredientDto>();
            CreateMap<IngredientDto, Ingredient>();
        }
    }
}
