﻿using AutoMapper;
using Pizzeria.DataAccess.Entities;
using Pizzeria.Services.Models;

namespace Pizzeria.Services.Profiles
{
    public class PizzaProfile : Profile
    {
        public PizzaProfile()
        {
            CreateMap<Pizza, PizzaDto>();
            CreateMap<PizzaDto, Pizza>();
        }
    }
}
