﻿using AutoMapper;
using Pizzeria.DataAccess.Entities;
using Pizzeria.Services.Models;

namespace Pizzeria.Services.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            //CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>()
                .ForMember(x=>x.Pizza, 
                    y=>y.MapFrom(source=>source.PizzaDto))
                .ReverseMap();
        }
    }
}
