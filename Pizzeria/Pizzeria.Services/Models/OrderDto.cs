﻿namespace Pizzeria.Services.Models
{
    public class OrderDto
    {
        public string Id { get; set; }

        public PizzaDto PizzaDto { get; set; }
    }
}
