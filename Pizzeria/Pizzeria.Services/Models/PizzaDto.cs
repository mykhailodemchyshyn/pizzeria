﻿namespace Pizzeria.Services.Models
{
    public class PizzaDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public string[] Ingredients { get; set; }
    }
}
