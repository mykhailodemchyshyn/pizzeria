﻿namespace Pizzeria.Services.Models
{
    public class IngredientDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }
    }
}
