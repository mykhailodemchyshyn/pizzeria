﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pizzeria.Services.Interfaces
{
    public interface IService<TModel> where TModel : class
    {
        public IEnumerable<TModel> GetAll();
        public Task<TModel> GetByIdAsync(string id);
        public void Add(TModel model);
        public void Update(TModel model);
    }
}
