﻿using Pizzeria.Services.Models;

namespace Pizzeria.Services.Interfaces
{
    public interface IIngredientService: IService<IngredientDto>
    {
    }
}
