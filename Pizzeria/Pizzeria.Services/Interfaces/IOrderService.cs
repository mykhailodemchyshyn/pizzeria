﻿using Pizzeria.Services.Models;

namespace Pizzeria.Services.Interfaces
{
    public interface IOrderService: IService<OrderDto>
    {
    }
}
