﻿using Pizzeria.Services.Models;

namespace Pizzeria.Services.Interfaces
{
    public interface IPizzaService : IService<PizzaDto>
    {
    }
}
