export interface IIngredients {
    ingredients: string[];
    price: number;
}