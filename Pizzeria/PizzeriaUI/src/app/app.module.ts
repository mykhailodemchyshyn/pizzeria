import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PizzaListComponent } from './pizzas/pizza-list.component';
import { RouterModule } from '@angular/router';
import { PizzaDetailComponent } from './pizzas/pizza-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    PizzaListComponent,
    PizzaDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'pizzas',  component: PizzaListComponent},
      { path: 'pizzas/:id',  component: PizzaDetailComponent},
      { path: '**', redirectTo: 'pizzas', pathMatch: 'full' }
    ]),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
