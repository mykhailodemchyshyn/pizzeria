import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { IPizza } from "../models/pizza";
import { PizzaService } from "../services/pizza.service";

@Component({
    selector: 'pm-pizzas',
    templateUrl: './pizza-list.component.html',
    styleUrls: ['./pizza-list.component.css']
})
export class PizzaListComponent implements OnInit, OnDestroy {
    pageTitle: string = 'Pizza List';
    sub!: Subscription;

    private _listFilter: string = '';
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredPizzas = this.performFilter(value);
    }

    filteredPizzas: IPizza[] = [];
    pizzas: IPizza[] = [];

    constructor(private pizzaService: PizzaService) {};

    performFilter(filterBy: string): IPizza[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.pizzas.filter((product: IPizza) =>
            product.name.toLocaleLowerCase().includes(filterBy));
    }

    replenishIngredients() {
        this.sub=this.pizzaService.replenishIngredients().subscribe();
    }

    ngOnInit(): void {
        this.sub = this.pizzaService.getPizzas().subscribe({
            next: pizzas => {
                this.pizzas = pizzas;
                this.filteredPizzas = this.pizzas;
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}