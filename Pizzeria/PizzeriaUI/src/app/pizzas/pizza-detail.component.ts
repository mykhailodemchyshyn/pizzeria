import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IIngredients } from '../models/ingredients';
import { IPizza } from '../models/pizza';
import { PizzaService } from '../services/pizza.service';

@Component({
  templateUrl: './pizza-detail.component.html',
  styleUrls: ['./pizza-detail.component.css']
})
export class PizzaDetailComponent implements OnInit {
  pageTitle: string = 'Pizza Detail';
  ingredients: IIngredients | undefined;
  errorMessage: string = '';
  sub!: Subscription;
  availabilityOfIngredients: boolean = false;
  cookedPizza: boolean = false;
  payedPizza!: IPizza;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private pizzaService: PizzaService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.sub = this.pizzaService.getPizzaDetail(id||"").subscribe({
        next: ingredients => {
            this.ingredients = ingredients;
            if(ingredients.price==undefined){
              this.pageTitle = "Missing ingredients";
            }
        },
        error: err => this.errorMessage = err
    });
  }

  onBack(): void {
    this.router.navigate(['/pizzas']);
  }

  takeIngredients(): void {
    alert("collecting ingredients");
    this.sub=this.pizzaService.takeIngredients(this.ingredients?.ingredients || []).subscribe({
      next: availabilityOfIngredients => {
        this.availabilityOfIngredients = availabilityOfIngredients;
      }
    })
  }

  cook(): void {
    alert("cooking pizza");
    const id = this.route.snapshot.paramMap.get('id');
    this.sub=this.pizzaService.cook(id || "").subscribe({
      next: cookedPizza => {
        this.cookedPizza = cookedPizza;
      }
    })
  }

  pay(): void {
    const pizzaId = this.route.snapshot.paramMap.get('id');
    this.sub=this.pizzaService.pay(pizzaId || "").subscribe({
      next: payedPizza => {
        this.payedPizza = payedPizza;
      }
    })
  }

}
