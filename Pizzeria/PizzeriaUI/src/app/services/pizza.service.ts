import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { IPizza } from "../models/pizza";
import { IIngredients } from "../models/ingredients";

@Injectable({
    providedIn: 'root'
})
export class PizzaService {

    private apiUrl: string = `${environment.apiUrl}/api`;

    constructor(private http: HttpClient) {}

    getPizzas(): Observable<IPizza[]> {
        return this.http.get<IPizza[]>(this.apiUrl+'/home');
    }

    getPizzaDetail(id: string): Observable<IIngredients> {
        return this.http.get<IIngredients>(this.apiUrl + '/home/'+ id);
    }

    takeIngredients(ingredients: string[]): Observable<boolean> {
        return this.http.post<boolean>(this.apiUrl + '/ingredient',  ingredients );
    }

    cook(id: string): Observable<boolean> {
        return this.http.get<boolean>(this.apiUrl + '/ingredient/' + id);
    }

    pay(id: string): Observable<IPizza> {
        return this.http.get<IPizza>(this.apiUrl + '/order/'+ id);
    }

    replenishIngredients() : Observable<void> {
        return this.http.get<any>(this.apiUrl + '/ingredient');
    }
}