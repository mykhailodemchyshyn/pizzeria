﻿using AutoMapper;
using Pizzeria.Api.Models;
using Pizzeria.Services.Models;

namespace Pizzeria.Api.Profiles
{
    public class IngredientsDtoProfile : Profile
    {
        public IngredientsDtoProfile()
        {
            CreateMap<IngredientDto, IngredientViewModel>();
            CreateMap<IngredientViewModel, IngredientDto>();
        }
    }
}
