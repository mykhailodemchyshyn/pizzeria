﻿using AutoMapper;
using Pizzeria.Api.Models;
using Pizzeria.Services.Models;

namespace Pizzeria.Api.Profiles
{
    public class OrderDtoProfile : Profile
    {
        public OrderDtoProfile()
        {
            CreateMap<OrderDto, OrderViewModel>();
            CreateMap<OrderViewModel, OrderDto>();
        }
    }
}
