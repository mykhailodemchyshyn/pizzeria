﻿using AutoMapper;
using Pizzeria.Api.Models;
using Pizzeria.Services.Models;

namespace Pizzeria.Api.Profiles
{
    public class PizzaDtoProfile : Profile
    {
        public PizzaDtoProfile()
        {
            CreateMap<PizzaDto, PizzaViewModel>();
            CreateMap<PizzaViewModel, PizzaDto>();
        }
    }
}
