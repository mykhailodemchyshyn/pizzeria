﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Pizzeria.Api.Models;
using Pizzeria.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Pizzeria.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IPizzaService _pizzaService;
        private readonly IIngredientService _ingredientService;
        private readonly IMapper _mapper;

        public HomeController(IPizzaService pizzaService, IIngredientService ingredientService, IMapper mapper)
        {
            _pizzaService = pizzaService;
            _ingredientService = ingredientService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult AllPizzas()
        {
            var pizzas = _pizzaService.GetAll();
            var mapPizzas = _mapper.Map<IEnumerable<PizzaViewModel>>(pizzas);

            return Ok(mapPizzas);
        }

        [HttpGet("{id}")]
        public IActionResult Pizza(string id)
        {
            var pizza = _pizzaService.GetByIdAsync(id).Result;
            var ingredients = _ingredientService.GetAll();

            var ingredientsForPizza = ingredients.Select(i => i.Name).Intersect(pizza.Ingredients);

            var missingIngredients = ingredients.Join(ingredientsForPizza,
                i => i.Name,
                j => j,
                (i, j) => new IngredientViewModel
                { Name = i.Name, Count = i.Count }).Where(m=>m.Count==0);

            if(missingIngredients.Any())
            {
                return Ok(new { ingredients = missingIngredients.Select(m=>m.Name) });
            }

            return Ok(new { ingredients = ingredientsForPizza, pizza.Price });
        }
    }
}
