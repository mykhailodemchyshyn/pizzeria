﻿using Microsoft.AspNetCore.Mvc;
using Pizzeria.Services.Interfaces;
using Pizzeria.Services.Models;
using System;

namespace Pizzeria.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IPizzaService _pizzaService;

        public OrderController(IOrderService orderService, IPizzaService pizzaService)
        {
            _orderService = orderService;
            _pizzaService = pizzaService;
        }

        [HttpGet("{id}")]
        public IActionResult CreateOrder(string id)
        {
            var pizza = _pizzaService.GetByIdAsync(id).Result;
            var order = new OrderDto { PizzaDto = pizza };
            for (var i = 0; i < 3; i++)
            {
                var checkPayment = PayForTheOrder();
                if (!checkPayment) continue;

                _orderService.Add(order);
                return Ok(pizza);
            }

            return Ok(null);
        }

        private bool PayForTheOrder()
        {
            var random = new Random();
            
            return random.Next(0,2)!=0;
        }
    }
}
