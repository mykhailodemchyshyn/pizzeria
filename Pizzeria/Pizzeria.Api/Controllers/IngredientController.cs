﻿using Microsoft.AspNetCore.Mvc;
using Pizzeria.Services.Interfaces;
using System.Linq;
using System.Threading;

namespace Pizzeria.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngredientController : ControllerBase
    {
        private readonly IIngredientService _ingredientService;
        private readonly IPizzaService _pizzaService;

        public IngredientController(IIngredientService ingredientService, IPizzaService pizzaService)
        {
            _ingredientService = ingredientService;
            _pizzaService = pizzaService;
        }

        [HttpPost]
        public IActionResult TakeIngredients([FromBody] string[] ingredients)
        {
            var ingredientsForPizza = _ingredientService.GetAll()
                .Join(ingredients,
                i => i.Name,
                j => j,
                (i, j) => i);

            foreach(var ingredient in ingredientsForPizza)
            {
                ingredient.Count--;
                _ingredientService.Update(ingredient);
                Thread.Sleep(3000);
            }

            return Ok(true);
        }

        [HttpGet("{id}")]
        public IActionResult CockingPizza(string id)
        {
            var pizza = _pizzaService.GetByIdAsync(id);

            if(pizza==null)
            {
                return NotFound();
            }

            //Thread.Sleep(60000);
            Thread.Sleep(6000);

            return Ok(true);
        }

        [HttpGet]
        public IActionResult ReplenishIngredients()
        {
            var ingredients = _ingredientService.GetAll();
            foreach(var ingredient in ingredients)
            {
                if(ingredient.Count<5)
                {
                    ingredient.Count += 5;
                    _ingredientService.Update(ingredient);
                }
            }

            return Ok();
        }
    }
}
