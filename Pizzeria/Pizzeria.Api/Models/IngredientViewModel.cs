﻿namespace Pizzeria.Api.Models
{
    public class IngredientViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }
    }
}
