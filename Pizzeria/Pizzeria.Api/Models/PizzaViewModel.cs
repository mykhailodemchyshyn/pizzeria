﻿namespace Pizzeria.Api.Models
{
    public class PizzaViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }
    }
}
