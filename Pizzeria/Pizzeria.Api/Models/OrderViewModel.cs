﻿namespace Pizzeria.Api.Models
{
    public class OrderViewModel
    {
        public string Id { get; set; }

        public PizzaViewModel PizzaViewModel { get; set; }
    }
}
