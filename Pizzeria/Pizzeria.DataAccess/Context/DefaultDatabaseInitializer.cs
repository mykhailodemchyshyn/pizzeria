﻿using MongoDB.Driver;
using Pizzeria.DataAccess.Entities;
using System.Linq;

namespace Pizzeria.DataAccess.Context
{
    static class DefaultDatabaseInitializer
    {
        public static void Initialize(IMongoDatabase database)
        {
            InitializeIngredients(database);
            InitializePizzas(database);
        }

        public static void InitializeIngredients(IMongoDatabase database)
        {
            var ingredients = database.GetCollection<Ingredient>("Ingredients").AsQueryable().ToList();
            if (ingredients.Any())
            {
                return;
            }

            database.GetCollection<Ingredient>("Ingredients").InsertManyAsync(new[]
            {
                new Ingredient { Name = "sauce", Count = 45 },
                new Ingredient { Name = "meat", Count = 30 },
                new Ingredient { Name = "chicken", Count = 20 },
                new Ingredient { Name = "cheese", Count = 47 },
                new Ingredient { Name = "mushrooms", Count = 10 },
                new Ingredient { Name = "tomato", Count = 13 },
                new Ingredient { Name = "salami", Count = 0 },
                new Ingredient { Name = "sausages", Count = 0 },
                new Ingredient { Name = "parmesan", Count = 7 },
                new Ingredient { Name = "pepper", Count = 3 },
                new Ingredient { Name = "onion", Count = 1 },
                new Ingredient { Name = "mozzarella", Count = 22 },
                new Ingredient { Name = "ham", Count = 9 }
            });
        }

        public static void InitializePizzas(IMongoDatabase database)
        {
            var res = database.GetCollection<Pizza>("Pizzas").AsQueryable().ToList();
            if (res.Any())
            {
                return;
            }

            database.GetCollection<Pizza>("Pizzas").InsertManyAsync(new[]
            {
                new Pizza { Name = "margarita", Price = 130, Ingredients = new[] { "mozzarella", "tomato", "sauce" }},
                new Pizza { Name = "carbonara", Price = 180, Ingredients = new[] {"cheese", "sauce", "ham", "sausages" }},
                new Pizza { Name = "salami", Price = 160, Ingredients = new[] {"cheese", "sauce", "salami" }},
                new Pizza { Name = "pepperoni", Price = 170, Ingredients = new[] { "mozzarella", "sauce", "pepper", "parmesan" }},
                new Pizza { Name = "capriccosa", Price = 175, Ingredients = new[] { "cheese", "sauce", "tomato", "meet", "mushrooms" }},
                new Pizza { Name = "polo", Price = 165, Ingredients = new[] { "mozzarella", "sauce", "tomato", "onion", "chicken" }}
            });
        }
    }
}
