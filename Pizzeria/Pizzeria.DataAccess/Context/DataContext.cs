﻿using MongoDB.Driver;

namespace Pizzeria.DataAccess.Context
{
    public class DataContext
    {
        public IMongoDatabase Database;

        public DataContext()
        {
            const string connectionString = "mongodb://localhost:27017/pizzeria";
            var connection = new MongoUrlBuilder(connectionString);
            var client = new MongoClient(connectionString);
            Database = client.GetDatabase(connection.DatabaseName);
            DefaultDatabaseInitializer.Initialize(Database);
        }
    }
}
