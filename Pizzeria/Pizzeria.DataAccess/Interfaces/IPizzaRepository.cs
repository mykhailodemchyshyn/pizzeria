﻿using Pizzeria.DataAccess.Entities;

namespace Pizzeria.DataAccess.Interfaces
{
    public interface IPizzaRepository : IRepository<Pizza>
    {
    }
}
