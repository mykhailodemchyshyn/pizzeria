﻿using Pizzeria.DataAccess.Entities;

namespace Pizzeria.DataAccess.Interfaces
{
    public interface IIngredientRepository : IRepository<Ingredient>
    {
    }
}
