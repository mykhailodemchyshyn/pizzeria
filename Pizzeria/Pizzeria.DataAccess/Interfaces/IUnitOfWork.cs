﻿namespace Pizzeria.DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        public IPizzaRepository PizzaRepository { get; }
        public IIngredientRepository IngredientRepository { get; }
        public IOrderRepository OrderRepository { get; }
    }
}
