﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pizzeria.DataAccess.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        public List<TEntity> FindAll();
        public Task<TEntity> GetByIdAsync(string id);
        public Task AddAsync(TEntity entity);
        public void Update(TEntity entity);
       
    }
}
