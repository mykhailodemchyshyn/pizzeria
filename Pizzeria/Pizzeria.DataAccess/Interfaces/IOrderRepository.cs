﻿using Pizzeria.DataAccess.Entities;

namespace Pizzeria.DataAccess.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
