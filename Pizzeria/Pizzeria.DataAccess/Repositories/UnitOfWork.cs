﻿using Pizzeria.DataAccess.Context;
using Pizzeria.DataAccess.Interfaces;

namespace Pizzeria.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _dataContext = new DataContext();
        private PizzaRepository _pizzaRepository;
        private IngredientRepository _ingredientRepository;
        private OrderRepository _orderRepository;

        public IPizzaRepository PizzaRepository => _pizzaRepository ??= new PizzaRepository(_dataContext);
        public IIngredientRepository IngredientRepository => _ingredientRepository ??= new IngredientRepository(_dataContext);
        public IOrderRepository OrderRepository => _orderRepository ??= new OrderRepository(_dataContext);

    }
}
