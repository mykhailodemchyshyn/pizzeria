﻿using MongoDB.Bson;
using MongoDB.Driver;
using Pizzeria.DataAccess.Context;
using Pizzeria.DataAccess.Entities;
using Pizzeria.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.DataAccess.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DataContext _dataContext;

        public OrderRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IMongoCollection<Order> Orders => _dataContext.Database.GetCollection<Order>("Orders");

        public async Task AddAsync(Order entity)
        {
            await Orders.InsertOneAsync(entity);
        }
        public List<Order> FindAll()
        {
            return (from order in Orders.AsQueryable() select order).ToList();
        }

        public async Task<Order> GetByIdAsync(string id)
        {
            return await Orders.Find(new BsonDocument("_id", new ObjectId(id))).FirstOrDefaultAsync();
        }

        public void Update(Order entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
