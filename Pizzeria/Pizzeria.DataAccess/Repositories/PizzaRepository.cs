﻿using MongoDB.Bson;
using MongoDB.Driver;
using Pizzeria.DataAccess.Context;
using Pizzeria.DataAccess.Entities;
using Pizzeria.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.DataAccess.Repositories
{
    public class PizzaRepository : IPizzaRepository
    {
        private readonly DataContext _dataContext;

        public PizzaRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IMongoCollection<Pizza> Pizzas => _dataContext.Database.GetCollection<Pizza>("Pizzas");

        public Task AddAsync(Pizza entity)
        {
            throw new System.NotImplementedException();
        }
        public List<Pizza> FindAll()
        {
            return (from pizza in Pizzas.AsQueryable() select pizza).ToList();
        }

        public async Task<Pizza> GetByIdAsync(string id)
        {
            return await Pizzas.Find(new BsonDocument("_id", new ObjectId(id))).FirstOrDefaultAsync();
        }

        public void Update(Pizza entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
