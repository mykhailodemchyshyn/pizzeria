﻿using MongoDB.Bson;
using MongoDB.Driver;
using Pizzeria.DataAccess.Context;
using Pizzeria.DataAccess.Entities;
using Pizzeria.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.DataAccess.Repositories
{
    public class IngredientRepository : IIngredientRepository
    {
        private readonly DataContext _dataContext;

        public IngredientRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IMongoCollection<Ingredient> Ingredients => _dataContext.Database.GetCollection<Ingredient>("Ingredients");

        public Task AddAsync(Ingredient entity)
        {
            throw new System.NotImplementedException();
        }
        public List<Ingredient> FindAll()
        {
            return (from ingredient in Ingredients.AsQueryable() select ingredient).ToList();
        }

        public async Task<Ingredient> GetByIdAsync(string id)
        {
            return await Ingredients.Find(new BsonDocument("_id", new ObjectId(id))).FirstOrDefaultAsync();
        }

        public async void Update(Ingredient entity)
        {
            await Ingredients.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(entity.Id)), entity);
        }
    }
}
