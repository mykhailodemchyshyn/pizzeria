﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Pizzeria.DataAccess.Entities
{
    public class Pizza
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Price")]
        public int Price { get; set; }

        [Display(Name = "IngredientsId")]
        public string[] Ingredients { get; set; }
    }
}
